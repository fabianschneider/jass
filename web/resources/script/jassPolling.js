/** Delay between requests to the server when polling. */
var pollDelay = 2000;
var polling = false;

function processPollEvent() {
    return function(data) {
        if (data.status == 'success' && polling) {
            setTimeout("poll()", pollDelay);
        }
    };
}

function poll() {
//    var element = document.getElementById('startBtn');
//    element.mgPoll = true;
    jsf.ajax.request('contentForm', null, {
            render: 'contentForm:gametable contentForm:gamecontrols contentForm:nextTurn contentForm:finishMessage contentForm:trump contentForm:teamPoints',
            execute:'@form',
            onevent: processPollEvent()
            });
}

function startAjaxPoll() {
    if(!polling){
        setTimeout("poll('"+pollDelay+"')", pollDelay);
        polling = true;
    }
}

function stopAjaxPoll(){
    polling = false;
}