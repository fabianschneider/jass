function processResultButtonClick() {
    return function(data) {
        if (data.status == 'success') {
            $('#results').fadeIn('fast');
        }
    };
}

$(document).ready(function() {
   
    $('#profilebutton').live('click',function(event){
        $('#profile').slideToggle('slow');
    })
    
    $('#results_button').live('click',function(event){
        if(!$('#results').is(':visible')){
            
            jsf.ajax.request('contentForm', null, {
                render: 'contentForm:results',
                execute:'@form',
                onevent: processResultButtonClick()
            });
        }
    })

    $('html').click(function() {
        if($('#results').is(':visible')){
            $('#results').fadeOut('fast')
        }
    });

    $('#results').click(function(event){
        event.stopPropagation();
    });
    
    if($('#messages').children('div').html()==''){
        $('#messages').hide();
    }else{
        $('#messages').show();
    }
    
    $('#messages').ajaxSuccess(function(){
        console.log('foo')
        if($('#messages').children('div').html()==''){
            $('#messages').hide();
        }else{
            $('#messages').show();
        }  
    })
    
});


