package ch.bfh.jass.test;

import ch.bfh.jass.exceptions.*;
import ch.bfh.jass.game.*;
import ch.bfh.jass.game.JassCardSet.CardColor;
import ch.bfh.jass.game.JassCardSet.CardRank;
import ch.bfh.jass.interfaces.IPlayer;
import ch.bfh.testutil.MockPlayer;
import java.util.ArrayList;
import java.util.List;
import org.junit.*;
import org.junit.rules.ExpectedException;

/**
 *
 * @author david
 */
public class GameTest {

    private class MockRule extends JassRule {

        private Player player;

        public MockRule(Player player) {
            super();
            this.player = player;
        }

        @Override
        public Player getStartPlayer() {
            return this.player;
        }
    }
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private IPlayer player1;
    private IPlayer player2;
    private IPlayer player3;
    private IPlayer player4;
    private IPlayer player5;
    private MockPlayer hans;
    private MockPlayer fritz;
    private MockPlayer juerg;
    private MockPlayer moritz;
    private MockRule mockrule;
    private Game game;
    private List<Card> handHans;
    private List<Card> handFritz;
    private List<Card> handJuerg;
    private List<Card> handMoritz;
    private Card herz6;
    private Card herz10;
    private Card schaufel6;
    private Card schaufel7;
    private Card ecke6;
    private Card herzAss;
    private Card herzBube;
    private Card kreuz10;

    @Before
    public void setUp() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException{
        this.player1 = new Player();
        this.player2 = new Player();
        this.player3 = new Player();
        this.player4 = new Player();
        this.player5 = new Player();

        this.hans = new MockPlayer();
        this.fritz = new MockPlayer();
        this.juerg = new MockPlayer();
        this.moritz = new MockPlayer();

        this.mockrule = new MockRule(this.hans);

        this.game = new Game(hans, mockrule);

        this.handHans = new ArrayList<Card>();
        this.handFritz = new ArrayList<Card>();
        this.handJuerg = new ArrayList<Card>();
        this.handMoritz = new ArrayList<Card>();

        this.herz6 = new Card(CardColor.HERZ, CardRank.SECHS);
        this.herz10 = new Card(CardColor.HERZ, CardRank.ZEHN);
        this.schaufel6 = new Card(CardColor.SCHAUFEL, CardRank.SECHS);
        this.schaufel7 = new Card(CardColor.SCHAUFEL, CardRank.SIEBEN);
        this.ecke6 = new Card(CardColor.ECKE, CardRank.SECHS);
        this.herzAss = new Card(CardColor.HERZ, CardRank.ASS);
        this.herzBube = new Card(CardColor.HERZ, CardRank.BUBE);
        this.kreuz10 = new Card(CardColor.KREUZ, CardRank.ZEHN);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGameAdmin() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException{
        Game jass = new Game(this.player1);
        junit.framework.Assert.assertEquals(this.player1, jass.getAdmin());
    }

    @Test
    public void testStartPlayer() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException, YouMustNotException {
        Game jass = new Game(this.player1);
        jass.addPlayer(this.player2, 1);
        jass.addPlayer(this.player3, 2);
        jass.addPlayer(this.player4, 3);
        jass.start(this.player1);

        Card startCard = new Card(CardColor.ECKE, CardRank.SIEBEN);

        for (Card card : jass.getRule().getAllCards()) {
            if (card.equals(startCard)) {
                Assert.assertEquals(card.getHolder(), jass.getCurrentPlayer());
            }
        }
    }

    @Test
    public void testRightCard() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException, NotYourCardException, IsNotPlayableException, GameOverException, NoTrumpfSetException, YouMustNotException, YouMustNotSetTrumpfException{
        Game jass = new Game(this.player1);
        jass.addPlayer(this.player2, 1);
        jass.addPlayer(this.player3, 2);
        jass.addPlayer(this.player4, 3);
        jass.start(this.player1);
        
        jass.setTrumpf(CardColor.HERZ, jass.getCurrentPlayer());

        if (jass.getPlayers().indexOf(jass.getCurrentPlayer()) == 3) {
            this.player5 = jass.getPlayers().get(0);
        } else {
            this.player5 = jass.getPlayers().get(3);
        }
        this.thrown.expect(NotYourCardException.class);
        jass.play(this.player5.getHandCards().get(0));
    }

    @Test
    public void testAddPlayers() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException {
        Game jass = new Game(this.player1);
        junit.framework.Assert.assertEquals(this.player1, jass.getPlayers().get(0));

        jass.addPlayer(this.player2, 1);
        junit.framework.Assert.assertEquals(this.player2, jass.getPlayers().get(1));
        jass.addPlayer(this.player3, 2);
        junit.framework.Assert.assertEquals(this.player3, jass.getPlayers().get(2));
        jass.addPlayer(this.player4, 3);
        junit.framework.Assert.assertEquals(this.player4, jass.getPlayers().get(3));

        this.thrown.expect(PlaceTakenException.class);
        jass.addPlayer(this.player5, 3);

    }

    @Test
    public void testSamePlayerAddedTwice() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException {
        Game jass = new Game(this.player1);
        this.thrown.expect(PlayerAlreadyInGameException.class);
        jass.addPlayer(this.player1, 1);
    }
    
    @Test
    public void testPlaceDoesNotExist() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException {
        Game jass = new Game(this.player1);
        this.thrown.expect(PlaceDoesNotExistException.class);
        jass.addPlayer(this.player1, 4);
    }

    @Test
    public void testTableCards() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException, NotYourCardException, IsNotPlayableException, GameOverException, NoTrumpfSetException, YouMustNotException, YouMustNotSetTrumpfException {
        Game jass = new Game(this.player1);
        jass.addPlayer(this.player2, 1);
        jass.addPlayer(this.player3, 2);
        jass.addPlayer(this.player4, 3);
        jass.start(this.player1);
        jass.setTrumpf(CardColor.HERZ, jass.getCurrentPlayer());
        Assert.assertNull(jass.getTable().getTableCards().get(0));
        Assert.assertNull(jass.getTable().getTableCards().get(1));
        Assert.assertNull(jass.getTable().getTableCards().get(2));
        Assert.assertNull(jass.getTable().getTableCards().get(3));
        Card playCard = jass.getCurrentPlayer().getHandCards().get(0);
        jass.play(playCard);
        Assert.assertNotNull(jass.getTable().getTableCards().get(playCard.getHolder().getPlace()));
        Assert.assertTrue(jass.getTable().getTableCards().contains(playCard));
        Assert.assertFalse(jass.getCurrentPlayer().getHandCards().contains(playCard));
    }

    @Test
    public void testPlay() throws PlayerAlreadyInGameException, NotYourCardException, IsNotPlayableException, GameOverException, PlaceTakenException, PlaceDoesNotExistException, NoTrumpfSetException, YouMustNotException {
        herz6.setHolder(hans);
        herz10.setHolder(hans);
        schaufel6.setHolder(fritz);
        schaufel7.setHolder(fritz);
        ecke6.setHolder(juerg);
        herzAss.setHolder(juerg);
        herzBube.setHolder(moritz);
        kreuz10.setHolder(moritz);

        handHans.add(herz6);
        handHans.add(herz10);

        handFritz.add(schaufel6);
        handFritz.add(schaufel7);

        handJuerg.add(ecke6);
        handJuerg.add(herzAss);

        handMoritz.add(herzBube);
        handMoritz.add(kreuz10);

        game.addPlayer(fritz, 1);
        game.addPlayer(juerg, 2);
        game.addPlayer(moritz, 3);
        game.start(this.hans);

        hans.setHand(new Hand(handHans));
        fritz.setHand(new Hand(handFritz));
        juerg.setHand(new Hand(handJuerg));
        moritz.setHand(new Hand(handMoritz));

        mockrule.setTrumpf(CardColor.ECKE);

        game.play(herz10);
        Assert.assertEquals(fritz, game.getCurrentPlayer());
        game.play(schaufel6);
        Assert.assertEquals(juerg, game.getCurrentPlayer());
        game.play(herzAss);
        Assert.assertEquals(moritz, game.getCurrentPlayer());
        game.play(herzBube);
        Assert.assertEquals(juerg, game.getCurrentPlayer());
        Assert.assertNull(game.getTable().getTableCards().get(0));
        Assert.assertNull(game.getTable().getTableCards().get(1));
        Assert.assertNull(game.getTable().getTableCards().get(2));
        Assert.assertNull(game.getTable().getTableCards().get(3));
    }
    
    @Test
    public void testNewRound() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException, NotYourCardException, IsNotPlayableException, GameOverException, NoTrumpfSetException, YouMustNotException, YouMustNotSetTrumpfException {
        this.player1 = new AIJassPlayer();
        this.player2 = new AIJassPlayer();
        this.player3 = new AIJassPlayer();
        this.player4 = new AIJassPlayer();
        
        Game jass = new Game(this.player1);
        jass.addPlayer(this.player2, 1);
        jass.addPlayer(this.player3, 2);
        jass.addPlayer(this.player4, 3);
        jass.start(this.player1);
        
        jass.setTrumpf(CardColor.HERZ, jass.getCurrentPlayer());
        
        IPlayer starter = jass.getRule().getStartPlayer();
        List<IPlayer> players = jass.getPlayers();
        IPlayer nextPlayer;
        
        if (starter.equals(players.get(players.size() - 1))) {
            nextPlayer = players.get(0);
        } else {
            nextPlayer = players.get(players.indexOf(starter) + 1);
        }
        
        for(int i = 0; i < 36; i++){
            jass.play(jass.getCurrentPlayer().chooseCard(jass.getRule(), jass.getTable()));    
        }
        Assert.assertEquals(nextPlayer, jass.getCurrentPlayer());
    }
    
    @Test
    public void testTrumpfIsSet() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException, NotYourCardException, IsNotPlayableException, GameOverException, NoTrumpfSetException, YouMustNotException{
        this.herz6.setHolder(hans);
        this.schaufel6.setHolder(fritz);
        this.ecke6.setHolder(juerg);
        this.herzBube.setHolder(moritz);

        this.handHans.add(herz6);
        this.handFritz.add(schaufel6);
        this.handJuerg.add(ecke6);
        this.handMoritz.add(herzBube);


        this.game.addPlayer(fritz, 1);
        this.game.addPlayer(juerg, 2);
        this.game.addPlayer(moritz, 3);
        this.game.start(hans);

        this.hans.setHand(new Hand(handHans));
        this.fritz.setHand(new Hand(handFritz));
        this.juerg.setHand(new Hand(handJuerg));
        this.moritz.setHand(new Hand(handMoritz));
        
        this.thrown.expect(NoTrumpfSetException.class);
        this.game.play(herz10);
    }
    
    @Test
    public void testGetTeamPlayer() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException{
        Game jass = new Game(this.player1);
        jass.addPlayer(this.player2, 1);
        jass.addPlayer(this.player3, 2);
        jass.addPlayer(this.player4, 3);
        
        Assert.assertEquals(this.player3, jass.getTeamPlayer(this.player1));
    }
}
