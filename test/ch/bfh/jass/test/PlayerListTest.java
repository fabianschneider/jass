/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package ch.bfh.jass.test;

import ch.bfh.jass.game.Player;
import ch.bfh.jass.game.PlayerList;
import ch.bfh.jass.exceptions.PlaceDoesNotExistException;
import ch.bfh.jass.exceptions.PlaceTakenException;
import ch.bfh.jass.exceptions.PlayerAlreadyInGameException;
import ch.bfh.jass.interfaces.IPlayer;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author fabianschneider
 */
public class PlayerListTest {

    @Test
    public void testNextPlayer() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException {
        Player player1 = new Player();
        Player player2 = new Player();
        Player player3 = new Player();
        Player player4 = new Player();

        PlayerList<IPlayer> players = new PlayerList<IPlayer>(4);
        players.addPlayer(0, player1);
        players.addPlayer(1, player2);
        players.addPlayer(2, player3);
        players.addPlayer(3, player4);
        
        players.setCurrent(player4);
        Assert.assertEquals(player1, players.next());
        
    }
}
