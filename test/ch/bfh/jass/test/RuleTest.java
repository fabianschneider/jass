/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package ch.bfh.jass.test;

import ch.bfh.jass.exceptions.PlaceDoesNotExistException;
import ch.bfh.jass.exceptions.PlaceTakenException;
import ch.bfh.jass.exceptions.PlayerAlreadyInGameException;
import ch.bfh.jass.game.JassCardSet.CardColor;
import ch.bfh.jass.game.JassCardSet.CardRank;
import ch.bfh.jass.game.*;
import ch.bfh.jass.interfaces.IPlayer;
import ch.bfh.jass.interfaces.IRule;
import ch.bfh.testutil.MockPlayer;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author fabianschneider
 */
public class RuleTest {

    public RuleTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testIsPlayable() {
        JassRule rule = new JassRule();
        rule.setTrumpf(CardColor.HERZ);

        MockPlayer hans = new MockPlayer();
        List<Card> handCards = new ArrayList<Card>();
        Card ecke6 = new Card(CardColor.ECKE, CardRank.SECHS);
        ecke6.setHolder(hans);
        Card ecke7 = new Card(CardColor.ECKE, CardRank.SIEBEN);
        ecke7.setHolder(hans);
        handCards.add(ecke6);
        handCards.add(ecke7);
        hans.setHand(new Hand(handCards));

        // empty table
        Table table = new Table(4);
        Assert.assertTrue(rule.isPlayable(new Card(CardColor.SCHAUFEL, CardRank.SECHS), table));
        table.addCard(0,new Card(CardColor.SCHAUFEL, CardRank.SECHS));
        // same color
        Assert.assertTrue(rule.isPlayable(new Card(CardColor.SCHAUFEL, CardRank.SIEBEN), table));
        table.addCard(1,new Card(CardColor.SCHAUFEL, CardRank.SIEBEN));
        // trumpf
        Assert.assertTrue(rule.isPlayable(ecke6, table));
        table.addCard(2,new Card(CardColor.ECKE, CardRank.SECHS));
        // no more same color
        Assert.assertTrue(rule.isPlayable(ecke6, table));
    }

    @Test
    public void testHighestCard() {
        JassRule rule = new JassRule();
        rule.setTrumpf(CardColor.HERZ);
        List<Card> cards = new ArrayList<Card>();
        cards.add(new Card(CardColor.SCHAUFEL, CardRank.BUBE));
        cards.add(new Card(CardColor.SCHAUFEL, CardRank.DAME));
        Assert.assertEquals(new Card(CardColor.SCHAUFEL, CardRank.DAME), rule.getHighestCard(cards));
        rule.setTrumpf(CardColor.SCHAUFEL);
        Assert.assertEquals(new Card(CardColor.SCHAUFEL, CardRank.BUBE), rule.getHighestCard(cards));
    }

    @Test
    public void testWinnerCard() {
        JassRule rule = new JassRule();
        Table table = new Table(4);
        table.addCard(0,new Card(CardColor.HERZ, CardRank.BUBE));
        table.addCard(1,new Card(CardColor.HERZ, CardRank.SECHS));
        table.addCard(2,new Card(CardColor.HERZ, CardRank.ASS));
        table.addCard(3,new Card(CardColor.SCHAUFEL, CardRank.SECHS));

        rule.setTrumpf(CardColor.ECKE);
        Assert.assertEquals(new Card(CardColor.HERZ, CardRank.ASS), rule.winnerCard(table));

        rule.setTrumpf(CardColor.HERZ);
        Assert.assertEquals(new Card(CardColor.HERZ, CardRank.BUBE), rule.winnerCard(table));

        rule.setTrumpf(CardColor.SCHAUFEL);
        Assert.assertEquals(new Card(CardColor.SCHAUFEL, CardRank.SECHS), rule.winnerCard(table));

    }

    @Test
    public void testHasWinner() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException {
        MockPlayer player1 = new MockPlayer();
        player1.setPoints(520);

        Player player2 = new Player();
        Player player3 = new Player();
        Player player4 = new Player();

        List<List<IPlayer>> teams = new ArrayList<List<IPlayer>>();
        List<IPlayer> team1 = new ArrayList<IPlayer>();
        List<IPlayer> team2 = new ArrayList<IPlayer>();
        team1.add(player1);
        team1.add(player2);
        team2.add(player3);
        team2.add(player4);
        teams.add(team1);
        teams.add(team2);
       
        IRule rule = new JassRule();

        Assert.assertTrue(rule.hasWinner(teams));
    }

    @Test
    public void testGetWinner() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException {
        MockPlayer player1 = new MockPlayer();
        player1.setPoints(520);

        MockPlayer player2 = new MockPlayer();
        player2.setPoints(521);
        
        Player player3 = new Player();
        Player player4 = new Player();

        List<List<IPlayer>> teams = new ArrayList<List<IPlayer>>();
        List<IPlayer> team1 = new ArrayList<IPlayer>();
        List<IPlayer> team2 = new ArrayList<IPlayer>();
        team1.add(player1);
        team1.add(player2);
        team2.add(player3);
        team2.add(player4);
        teams.add(team1);
        teams.add(team2);

        IRule rule = new JassRule();
        
        Assert.assertEquals(player2, rule.getWinner(teams).get(0));
        Assert.assertEquals(player4, rule.getWinner(teams).get(1));
    }
    
    @Test
    public void testSortCards(){
        IRule rule = new JassRule();
        Hand hand = rule.getHand();
        
        for(Card card : hand.getCards()){
            System.out.println(card.getRank());
        }
    }
}
