/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package ch.bfh.jass.test;

import ch.bfh.jass.game.Card;
import ch.bfh.jass.game.JassCardSet.CardColor;
import ch.bfh.jass.game.JassCardSet.CardRank;
import ch.bfh.jass.game.JassRule;
import ch.bfh.jass.game.Player;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author fabianschneider
 */
public class PlayerTest {

    public PlayerTest() {
    }

    @Test
    public void testEvaluateWinnerStock() {
        JassRule rule = new JassRule();
        rule.setTrumpf(CardColor.HERZ);

        List<Card> cards = new ArrayList<Card>();

        Card ecke6 = new Card(CardColor.ECKE, CardRank.SECHS);
        Card herzAss = new Card(CardColor.HERZ, CardRank.ASS);
        Card herzBube = new Card(CardColor.HERZ, CardRank.BUBE);
        Card kreuz10 = new Card(CardColor.KREUZ, CardRank.ZEHN);

        cards.add(ecke6);
        cards.add(herzAss);
        cards.add(herzBube);
        cards.add(kreuz10);

        Player hans = new Player();
        hans.addToWinnerStock(cards);
        hans.evaluateWinnerStock(rule);
        
        Assert.assertTrue(hans.getWinnerStock().isEmpty());
        Assert.assertTrue(hans.getPoints() == 41);


    }
}
