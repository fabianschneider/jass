package ch.bfh.jass.test;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
import ch.bfh.jass.game.Card;
import ch.bfh.jass.game.Game;
import ch.bfh.jass.game.JassCardSet;
import ch.bfh.jass.game.Player;
import ch.bfh.jass.exceptions.PlaceDoesNotExistException;
import ch.bfh.jass.exceptions.PlaceTakenException;
import ch.bfh.jass.exceptions.PlayerAlreadyInGameException;
import ch.bfh.jass.exceptions.YouMustNotException;
import ch.bfh.jass.interfaces.ICardSet;
import ch.bfh.jass.interfaces.IPlayer;
import java.util.ArrayList;
import java.util.Collection;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author fabianschneider
 */
public class DealingOutTest {

    Player player1;
    Player player2;
    Player player3;
    Player player4;

    @Before
    public void setUp() throws Exception {
        this.player1 = new Player();
        this.player2 = new Player();
        this.player3 = new Player();
        this.player4 = new Player();
    }

    @Test
    public void testNineCards() {
        ICardSet cardset = new JassCardSet();
        Assert.assertEquals(cardset.getCards(9).size(), 9);
    }

    @Test
    public void testDifferentHands() throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException, YouMustNotException {
        Collection<Card> handCards = new ArrayList<Card>();
        Game jass = new Game(this.player1);
        jass.addPlayer(this.player2, 1);
        jass.addPlayer(this.player3, 2);
        jass.addPlayer(this.player4, 3);

        jass.start(this.player1);

        for (IPlayer player : jass.getPlayers()) {
            handCards.addAll(player.getHandCards());
        }

        Assert.assertEquals(36, handCards.size());
        for (Card checkCard : handCards) {
            int count = 0;
            for (Card card : handCards) {
                if (checkCard.equals(card)) {
                    count++;
                }
            }
            Assert.assertEquals(1, count);
        }
    }
}
