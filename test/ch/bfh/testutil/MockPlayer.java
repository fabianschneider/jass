/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package ch.bfh.testutil;

import ch.bfh.jass.exceptions.NotYourCardException;
import ch.bfh.jass.game.Card;
import ch.bfh.jass.game.Hand;
import ch.bfh.jass.game.JassCardSet.CardColor;
import ch.bfh.jass.game.Player;

/**
 *
 * @author fabianschneider
 */
public class MockPlayer extends Player {

    private Hand hand;
    private int points;
    
    public MockPlayer(){
        super();
        this.points = 0;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public Hand getHand() {
        return this.hand;
    }

    @Override
    public boolean hasColorInHand(CardColor color) {
        return this.hand.hasColor(color);
    }
    
    @Override
    public Card getCard(Card card) throws NotYourCardException{
        return this.hand.getCard(card);
    }
    
    public void setPoints(int points){
        this.points = points;
    }
    
    @Override
    public int getPoints(){
        return this.points;
    }
}
