package ch.bfh.jass.exceptions;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class PlaceDoesNotExistException extends Exception {

    public PlaceDoesNotExistException(String msg) {
        super(msg);
    }
}
