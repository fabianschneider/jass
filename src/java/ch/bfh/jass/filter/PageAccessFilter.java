/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package ch.bfh.jass.filter;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class PageAccessFilter implements Filter {

    FilterConfig fc;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        fc = filterConfig;
    }

    /**
     * Redirect on login when user isn't logged in.
     * @param req
     * @param resp
     * @param chain
     * @throws IOException
     * @throws ServletException 
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession(false);
        String pageRequested = request.getRequestURI().toString();

        if (session == null) {
            // will create a new session
            session = request.getSession(true);
            response.sendRedirect("login.xhtml");
            System.out.println("not session");
        } else if ((session.getAttribute("AUTH") == null) && (pageRequested.contains("game.xhtml"))) {
            System.out.println("not auth");
            response.sendRedirect("login.xhtml");
        } else if(pageRequested.contains("gameOverview.xhtml")){
            response.sendRedirect("game.xhtml");
        } else if(pageRequested.contains("gameTable.xhtml")){
            response.sendRedirect("game.xhtml");
        } else if(pageRequested.contains("template.xhtml")){
            response.sendRedirect("game.xhtml");
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}