package ch.bfh.jass.listener;

import ch.bfh.jass.controller.MainBean;
import java.util.Locale;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.servlet.http.HttpSession;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class LanguageChangedActionEvent implements ActionListener{
    
    /**
     * Switch the locale.
     * @param e
     * @throws AbortProcessingException 
     */
    @Override
    public void processAction(ActionEvent e)
	throws AbortProcessingException{
	System.out.println("-------------------------");
	System.out.println("Change language 2: "+e.getComponent().getId());
	String language = e.getComponent().getId();
	FacesContext context = FacesContext.getCurrentInstance();
	Locale locale = new Locale(language);
	context.getViewRoot().setLocale(locale);
	System.out.println("Changed to locale 2: "+locale);
	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);	
	MainBean lb = (MainBean)(session.getAttribute("mainBean"));
	lb.setLocale(language);
	System.out.println("-------------------------");
    }
}
