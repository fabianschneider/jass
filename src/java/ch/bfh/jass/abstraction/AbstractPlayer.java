package ch.bfh.jass.abstraction;

import ch.bfh.jass.exceptions.NotYourCardException;
import ch.bfh.jass.game.Card;
import ch.bfh.jass.game.Hand;
import ch.bfh.jass.game.JassCardSet.CardColor;
import ch.bfh.jass.game.Table;
import ch.bfh.jass.interfaces.IPlayer;
import ch.bfh.jass.interfaces.IRule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public abstract class AbstractPlayer implements IPlayer {

    /**
     * Hand of the player
     */
    protected Hand hand;
    /**
     * The cards the player won
     */
    protected List<Card> winnerStock = new ArrayList<Card>();
    /**
     * The points the player won
     */
    protected int points;
    /**
     * The place where the player sits
     */
    protected int place;
    /**
     * The username of the player
     */
    protected String username;

    /**
     * Returns the cards of player's hand.
     *
     * @return
     */
    @Override
    public List<Card> getHandCards() {
        if (this.hand == null) {
            return Collections.unmodifiableList(new ArrayList<Card>());
        } else {
            return this.hand.getCards();
        }
    }

    /**
     * Returns a specified card of the player.
     *
     * @param card
     * @return
     * @throws NotYourCardException
     */
    @Override
    public Card getCard(Card card) throws NotYourCardException {
        return this.hand.getCard(card);
    }

    /**
     * Plays a card.
     *
     * @param card
     * @return
     */
    @Override
    public boolean playCard(Card card) {
        return this.hand.removeCard(card);
    }

    /**
     * Verifies if the player has card of a specified color.
     *
     * @param color
     * @return
     */
    @Override
    public boolean hasColorInHand(CardColor color) {
        return this.hand.hasColor(color);
    }

    /**
     * Returns the cards of a specified color.
     *
     * @param color
     * @return
     */
    @Override
    public List<Card> getCardsByColor(CardColor color) {
        return this.hand.getCardsByColor(color);
    }

    /**
     * Verfies if the hand of the player is empty.
     *
     * @return
     */
    @Override
    public boolean hasCards() {
        return !this.getHandCards().isEmpty();
    }

    /**
     * Adds a list of cards to the winnerstock.
     *
     * @param cards
     */
    @Override
    public void addToWinnerStock(List<Card> cards) {
        this.winnerStock.addAll(cards);
    }

    /**
     * Evalutes the points of the cards in the winnerstock.
     *
     * @param rule
     */
    @Override
    public void evaluateWinnerStock(IRule rule) {
        for (Card card : this.winnerStock) {
            this.points += rule.getPoints(card);
        }
        this.winnerStock.clear();
    }
    /**
     * Returns the cards in the winnerstock.
     * @return 
     */
    @Override
    public List<Card> getWinnerStock() {
        return Collections.unmodifiableList(this.winnerStock);
    }
    
    @Override
    public int getPoints() {
        return points;
    }

    @Override
    public boolean isAI() {
        return false;
    }
    /**
     * Child Classes has to implement this method.
     * @param rule
     * @param table
     * @return 
     */
    @Override
    public Card chooseCard(IRule rule, Table table) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    /**
     * Child classes has to implement this method.
     * @return 
     */
    @Override
    public CardColor chooseTrumpf() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getPlace() {
        return this.place;
    }

    @Override
    public void setPlace(int place) {
        this.place = place;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void clearHand() {
        this.hand = null;
    }
    
    /**
     * Adds some points to the player if the player had the last trick and if
     * the rule contains points for the last trick.
     * @param rule 
     */
    @Override
    public void lastTrick(IRule rule) {
        this.points += rule.getLastTrickPoints();
    }
}
