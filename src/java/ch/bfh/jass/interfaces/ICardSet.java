package ch.bfh.jass.interfaces;

import ch.bfh.jass.game.Card;
import java.util.List;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public interface ICardSet {
    
    public List<Card> getCards(int numOfCards);
    public List<Card> getAllCards();
    public void resetCardSet();
    
}
