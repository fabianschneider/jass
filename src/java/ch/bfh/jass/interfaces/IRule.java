package ch.bfh.jass.interfaces;

import ch.bfh.jass.game.Card;
import ch.bfh.jass.game.Hand;
import ch.bfh.jass.game.Table;
import java.util.List;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public interface IRule {

    public ICardSet getCardSet();

    public Hand getHand();

    public int getNumberOfPlayers();

    public int getNumberOfTableCards();

    public int getNumberOfTeams();

    public IPlayer getStartPlayer();

    public List<Card> getAllCards();

    public boolean isPlayable(Card card, Table table);

    public Card winnerCard(Table table);

    public IPlayer getStarter();

    public void setStarter(IPlayer player);

    public int getPoints(Card card);

    public boolean hasWinner(List<List<IPlayer>> teams);

    public List<IPlayer> getWinner(List<List<IPlayer>> teams);

    public void resetCardSet();

    public int[] countPoints(List<List<IPlayer>> teams);
    
    public int getLastTrickPoints();
}
