package ch.bfh.jass.interfaces;

import ch.bfh.jass.game.Card;
import ch.bfh.jass.game.Table;
import ch.bfh.jass.exceptions.NotYourCardException;
import ch.bfh.jass.game.JassCardSet.CardColor;
import java.util.List;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public interface IPlayer {

    public void initHand(IRule rule);

    public boolean isAI();

    public List<Card> getHandCards();

    public Card getCard(Card card) throws NotYourCardException;
    
    public Card chooseCard(IRule rule, Table table);
    
    public CardColor chooseTrumpf();

    public boolean playCard(Card card);

    public boolean hasColorInHand(CardColor color);

    public List<Card> getCardsByColor(CardColor color);

    public boolean hasCards();

    public void addToWinnerStock(List<Card> cards);

    public void evaluateWinnerStock(IRule rule);

    public List<Card> getWinnerStock();

    public int getPoints();
    
    public int getPlace();
    
    public void setPlace(int place);
    
    public String getUsername();
    
    public void setUsername(String username);
    
    public void clearHand();
    
    public void lastTrick(IRule rule);
}
