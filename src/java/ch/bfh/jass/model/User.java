/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package ch.bfh.jass.model;

/**
 *
 * @author david
 */
public class User {
    
    private int userID;

    private String username;

    private String password;
    
    private String fullName;
    
    private int points;
    
    protected User(int userID, String username, String password, String fullName){
	this.userID = userID;
        this.username = username;
        this.password = password;
        this.fullName = fullName;
        this.points = 0;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
