/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bfh.jass.model;

/**
 *
 * @author david
 */
public class HistoryPlayer {
    
    private int userID;

    private String username;
    
    private int points;
    
    protected HistoryPlayer(int userID, String username){
	this.userID = userID;
        this.username = username;
        this.points = 0;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
