package ch.bfh.jass.model;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class MessageFactory {
    
    /**
     * Gets the localized message from the MessageBundle and return the FacesMessage
     * @param severity
     * @param key
     * @param params
     * @return a FacesMessage with the given parameters
     */
    public static FacesMessage getMessage(FacesMessage.Severity severity, String key, Object... params) {
        FacesContext context = FacesContext.getCurrentInstance();
        String name = context.getApplication().getMessageBundle();
        if (name == null) {
            name = FacesMessage.FACES_MESSAGES;
        }
        Locale locale = context.getViewRoot().getLocale();
        ResourceBundle bundle = ResourceBundle.getBundle(name, locale);
        String summary = "???" + key + "???";
        String detail = null;
        try {
            summary = MessageFormat.format(bundle.getString(key), params);
            detail = MessageFormat.format(bundle.getString(key + "_detail"), params);
        } catch (MissingResourceException e) {
        }
        return new FacesMessage(severity, summary, detail);
    }

    /**
     * Adds an informational message to the faces context. 
     * @param key
     * @param params 
     */
    public static void info(String key, Object... params) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, getMessage(FacesMessage.SEVERITY_INFO, key, params));
    }
    
    /**
     * Adds an error message to the faces context. 
     * @param key
     * @param params 
     */
    public static void error(String key, Object... params) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, getMessage(FacesMessage.SEVERITY_ERROR, key, params));
    }
    
    /**
     * @param key
     * @return the localized text of the given key
     */
    public static String getText(String key){
        FacesContext context = FacesContext.getCurrentInstance();
        Locale locale = context.getViewRoot().getLocale();
        ResourceBundle bundle = ResourceBundle.getBundle("ch.bfh.jass.Texts", locale);
        return bundle.getString(key);
    }
}
