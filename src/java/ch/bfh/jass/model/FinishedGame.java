package ch.bfh.jass.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class FinishedGame {

    /**
     * The unique id of the game
     */
    private int gameID;
    /**
     * The user this game is according to.
     */
    private User user;
    /**
     * The team id of the user
     *
     */
    private int teamID;
    /**
     * The teams in this game.
     */
    private Map<Integer, Team> teams = new HashMap<Integer, Team>();

    /**
     * Class Constructor
     *
     * @param gameID
     * @param user
     */
    public FinishedGame(int gameID, User user) {
        this.gameID = gameID;
        this.user = user;
    }

    /**
     * Adds a player to the game.
     *
     * @param userToAdd
     * @param teamID
     */
    public void addPlayer(HistoryPlayer userToAdd, int teamID) {

        if (this.user.getUserID() == userToAdd.getUserID()) {
            this.teamID = teamID;
        }

        if (this.teams.containsKey(teamID)) {
            this.teams.get(teamID).addPlayer(userToAdd);
        } else {
            Team team = new Team(teamID);
            team.addPlayer(userToAdd);
            this.teams.put(teamID, team);
        }
    }
    
    /**
     * Returns the teams of the game. The game of the requestet user is on the
     * index 0.
     * @return 
     */
    public Team[] getTeams() {

        Team[] teamArray = new Team[this.teams.size()];
        teamArray[0] = this.teams.get(this.teamID);

        int i = 1;
        for (int teamid : this.teams.keySet()) {
            if (teamid != this.teamID && i < this.teams.size()) {
                teamArray[i] = this.teams.get(teamid);
                i++;
            }
        }
        return teamArray;
    }
    /**
     * Verifies if the requested user's team has won the game.
     * @return 
     */
    public boolean isHasWon() {
        Team[] teamArray = this.getTeams();
        for (int i = 1; i < teamArray.length; i++) {
            if (teamArray[0].getTeamPoints() < teamArray[i].getTeamPoints()) {
                return false;
            }
        }
        return true;
    }
}
