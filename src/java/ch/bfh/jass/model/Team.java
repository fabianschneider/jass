package ch.bfh.jass.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class Team {

    /**
     * The unique id of the game
     */
    private int teamID;
    /**
     * The players in this game
     */
    private List<HistoryPlayer> players = new ArrayList<HistoryPlayer>();

    /**
     * Class Constructor
     *
     * @param teamID
     */
    public Team(int teamID) {
        this.teamID = teamID;
    }

    /**
     * Adds a player to the team.
     *
     * @param user
     */
    public void addPlayer(HistoryPlayer user) {
        if (!this.players.contains(user)) {
            this.players.add(user);
        }
    }

    /**
     * Returns the players of the team.
     *
     * @return
     */
    public String getTeamPlayers() {
        String playersString = "";
        for (HistoryPlayer user : this.players) {
            if (user.equals(this.players.get(0))) {
                playersString = user.getUsername();
            } else {
                playersString = playersString + " & " + user.getUsername();
            }
        }
        return playersString;
    }

    /**
     * Returns the points of the team.
     *
     * @return
     */
    public int getTeamPoints() {
        int points = 0;
        for (HistoryPlayer user : this.players) {
            points += user.getPoints();
        }
        return points;
    }

    /**
     * Returns the id of the team.
     *
     * @return
     */
    public int getTeamID() {
        return this.teamID;
    }
}
