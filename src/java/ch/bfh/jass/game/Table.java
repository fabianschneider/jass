package ch.bfh.jass.game;

import ch.bfh.jass.game.JassCardSet.CardColor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class Table {

    /**
     * The number of cards on the table
     */
    private int tableSize;
    /**
     * The cards on the table
     */
    private Card[] tableCards;
    /**
     * The position of the first deposited card
     */
    private int firstCardPlace = -1;

    /**
     * Class Constructor
     *
     * @param tablesize
     */
    public Table(int tablesize) {
        this.tableSize = tablesize;
        this.tableCards = new Card[this.tableSize];
    }

    /**
     * Returns the cards on the table as a list.
     *
     * @return
     */
    public List<Card> getTableCards() {
        List<Card> cards = new ArrayList<Card>();
        for (Card card : this.tableCards) {
            cards.add(card);
        }
        return Collections.unmodifiableList(cards);
    }

    /**
     * Returns the cards on the table as an array.
     *
     * @return
     */
    public Card[] getTableCardsArray() {
        return this.tableCards.clone();
    }

    /**
     * Returns the card on a specified index.
     *
     * @param index
     * @return
     */
    public Card getCard(int index) {
        return this.tableCards[index];
    }

    /**
     * Adds a card on the table.
     *
     * @param position
     * @param card
     */
    public void addCard(int position, Card card) {
        if (this.firstCardPlace == -1) {
            this.firstCardPlace = position;
        }
        this.tableCards[position] = card;
    }

    /**
     * Returns number of cards on the table.
     *
     * @return
     */
    public int getSize() {
        int count = 0;
        for (Card card : this.tableCards) {
            if (card != null) {
                count++;
            }
        }
        return count;
    }

    /**
     * Returns all cards of specified color.
     *
     * @param color
     * @return
     */
    public List<Card> getCardsByColor(CardColor color) {
        List<Card> cardsByColor = new ArrayList<Card>();
        for (Card card : this.tableCards) {
            if (card.getColor() == color) {
                cardsByColor.add(card);
            }
        }
        return cardsByColor;
    }

    /**
     * Verifies if the table contains a card with a specified color.
     *
     * @param color
     * @return
     */
    public boolean hasColor(CardColor color) {
        return this.getCardsByColor(color).size() > 0;
    }

    /**
     * Moves the cards on the table to the winnerstock of the round winner.
     * Resets the place of the first deposited card.
     *
     * @return
     */
    public List<Card> clearTable() {
        List<Card> winnerStock = this.getTableCards();
        this.tableCards = new Card[this.tableSize];
        this.firstCardPlace = -1;
        return winnerStock;
    }

    /**
     * Returns the place of the first deposited place.
     *
     * @return
     */
    public int getFirstCardPlace() {
        return this.firstCardPlace;
    }
}
