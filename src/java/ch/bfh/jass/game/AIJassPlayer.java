package ch.bfh.jass.game;

import ch.bfh.jass.abstraction.AbstractPlayer;
import ch.bfh.jass.game.JassCardSet.CardColor;
import ch.bfh.jass.interfaces.IRule;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class AIJassPlayer extends AbstractPlayer {

    /**
     * Class Constructor
     */
    public AIJassPlayer() {
        this.points = 0;
    }

    /**
     * Initializes the hand of the AIJassPlayer
     *
     * @param rule
     */
    @Override
    public void initHand(IRule rule) {
        this.hand = rule.getHand();
        this.hand.setHolder(this);
    }

    /**
     * Chooses a playable card.
     *
     * @param rule
     * @param table
     * @return a playable card according to the specified rule
     */
    @Override
    public Card chooseCard(IRule rule, Table table) {
        for (Card card : super.getHandCards()) {
            if (rule.isPlayable(card, table)) {
                return card;
            }
        }
        throw new RuntimeException("Something went wrong!");
    }
    
    /**
     * Choose the trumpf color.
     * @return the chosen trumpf color
     */
    public CardColor chooseTrumpf() {
        int[] colorCount = new int[CardColor.values().length];
        int bestColorValue = 0;
        int bestColorIndex = 0;

        colorCount[0] = this.hand.getCardsByColor(CardColor.HERZ).size();
        colorCount[1] = this.hand.getCardsByColor(CardColor.KREUZ).size();
        colorCount[2] = this.hand.getCardsByColor(CardColor.SCHAUFEL).size();
        colorCount[3] = this.hand.getCardsByColor(CardColor.ECKE).size();

        for (int i = 0; i < CardColor.values().length; i++) {
            if (colorCount[i] > bestColorValue) {
                bestColorValue = colorCount[i];
                bestColorIndex = i;
            }
        }

        if (bestColorIndex == 0) {
            return CardColor.HERZ;
        } else if (bestColorIndex == 1) {
            return CardColor.KREUZ;
        } else if (bestColorIndex == 2) {
            return CardColor.SCHAUFEL;
        } else {
            return CardColor.ECKE;
        }
    }
    
    /**
     * Verfies this player as an AIPlayer.
     * @return boolean
     */
    @Override
    public boolean isAI() {
        return true;
    }
}
