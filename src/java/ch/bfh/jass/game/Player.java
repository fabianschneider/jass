package ch.bfh.jass.game;

import ch.bfh.jass.abstraction.AbstractPlayer;
import ch.bfh.jass.interfaces.IRule;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class Player extends AbstractPlayer {
    
    public Player() {
        this.points = 0;
    }
    
    /**
     * Initializes the hand of the player.
     * @param rule 
     */
    @Override
    public void initHand(IRule rule) {
        this.hand = rule.getHand();
        this.hand.setHolder(this);
    }
}
