package ch.bfh.jass.game;

import ch.bfh.jass.interfaces.ICardSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class JassCardSet implements ICardSet {
    
    private List<Card> cards = new ArrayList<Card>();
    
    public static enum CardColor { HERZ, SCHAUFEL, ECKE, KREUZ }
    public static enum CardRank { SECHS, SIEBEN, ACHT, NEUN, ZEHN, BUBE, DAME, KOENIG, ASS }
    
    /**
     * Class Constructor
     */
    public JassCardSet(){ 
        
        for(CardColor color : CardColor.values()){
            for(CardRank rank : CardRank.values()){
                Card card = new Card(color, rank);
                this.cards.add(card);
            }
        }     
    }
    
    /**
     * Get a number of cards of the cardset.
     * @param numOfCards
     * @return a specified number of cards
     */
    @Override
    public List<Card> getCards(int numOfCards) {
        List<Card> handCards = new ArrayList<Card>();
        while (handCards.size() < numOfCards) {            
            Card choosen_card = this.cards.get(this.rndIndex());
            if(!choosen_card.isInHand()){
                handCards.add(choosen_card);
                choosen_card.dealedOut();
            }
        }
        return handCards;
    }
    
    /**
     * @return all cards of the cardset
     */
    @Override
    public List getAllCards() {
        return Collections.unmodifiableList(this.cards);
    }
    
    /**
     * Resets all informations in the cards.
     */
    @Override
    public void resetCardSet() {
        for(Card card : this.cards){
            card.resetCardFlags();
        }
    }
    
    /**
     * @return a random index of the cardset.
     */
    private int rndIndex() {
        Random rand = new Random();
        return rand.nextInt(this.cards.size());
    }    
}
