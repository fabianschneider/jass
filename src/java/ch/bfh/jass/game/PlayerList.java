package ch.bfh.jass.game;

import ch.bfh.jass.exceptions.PlaceDoesNotExistException;
import ch.bfh.jass.exceptions.PlaceTakenException;
import ch.bfh.jass.exceptions.PlayerAlreadyInGameException;
import java.util.ArrayList;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class PlayerList<IPlayer> extends ArrayList<IPlayer> {

    private IPlayer current;
    private int numberOfPlayers;

    /**
     * Class Constructor
     *
     * @param numberOfPlayers
     */
    public PlayerList(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
        for (int i = 0; i < this.numberOfPlayers; i++) {
            this.add(null);
        }
    }

    public IPlayer next() {
        if (this.current.equals(this.get(this.size() - 1))) {
            this.current = this.get(0);
        } else {
            this.current = this.get(this.indexOf(this.current) + 1);
        }
        return this.current;
    }

    public IPlayer getCurrent() {
        return current;
    }

    public void setCurrent(IPlayer current) {
        this.current = current;
    }

    public void addPlayer(int i, IPlayer player) throws PlayerAlreadyInGameException, PlaceTakenException, PlaceDoesNotExistException {
        if (i < this.numberOfPlayers) {
            if (!this.contains(player)) {
                if (this.get(i) == null) {
                    super.remove(i);
                    super.add(i, player);
                } else {
                    throw new PlaceTakenException("The place" + i + " is taken!");
                }
            } else {
                throw new PlayerAlreadyInGameException("Player is already in game.");
            }
        } else {
            throw new PlaceDoesNotExistException("The place" + i + " does not exist!");
        }
    }

    /**
     * Returns the number of taken places in the list.
     *
     * @return
     */
    public int getNumberOfTakenPlaces() {
        int count = 0;
        for (IPlayer player : this) {
            if (player != null) {
                count++;
            }
        }
        return count;
    }

    /**
     * Returns the number of free places in the list.
     *
     * @return
     */
    public int[] getFreePlaces() {
        int[] freePlaces = new int[this.numberOfPlayers - this.getNumberOfTakenPlaces()];
        int position = 0;
        for (int i = 0; i < this.numberOfPlayers; i++) {
            if (this.get(i) == null) {
                freePlaces[position] = i;
                position++;
            }
        }
        return freePlaces;
    }
}
