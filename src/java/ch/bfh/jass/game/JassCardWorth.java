package ch.bfh.jass.game;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class JassCardWorth {
    public final static int VALUE_6 = 1;
    public final static int VALUE_7 = 2;
    public final static int VALUE_8 = 3;
    public final static int VALUE_9 = 4;
    public final static int VALUE_10 = 5;
    public final static int VALUE_BUBE = 6;
    public final static int VALUE_DAME = 7;
    public final static int VALUE_KOENIG = 8;
    public final static int VALUE_ASS = 9;
}
