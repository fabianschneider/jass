package ch.bfh.jass.game;

import ch.bfh.jass.exceptions.NotYourCardException;
import ch.bfh.jass.game.JassCardSet.CardColor;
import ch.bfh.jass.interfaces.IPlayer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class Hand {

    /**
     * Cards in the hand.
     */
    private List<Card> cards = new ArrayList<Card>();

    /**
     * Class Constructor
     *
     * @param cards
     */
    public Hand(List<Card> cards) {
        this.cards = cards;
    }

    /**
     * @return an unmodifiable list of the cards in the hand
     */
    public List<Card> getCards() {
        return Collections.unmodifiableList(this.cards);
    }

    /**
     * Sets the holder for this hand and every card in it.
     *
     * @param player
     */
    public void setHolder(IPlayer player) {
        for (Card card : this.cards) {
            card.setHolder(player);
        }
    }

    /**
     * Returns a specified card in the hand.
     *
     * @param card
     * @return the specified card
     * @throws NotYourCardException
     */
    public Card getCard(Card card) throws NotYourCardException {
        if (!this.cards.contains(card)) {
            throw new NotYourCardException("Not your card buddy!");
        }
        return this.cards.get(this.cards.indexOf(card));
    }

    /**
     * Removes a specified card in the hand.
     *
     * @param card
     * @return the removed card
     */
    public boolean removeCard(Card card) {
        return this.cards.remove(card);
    }

    /**
     * Checks the hand for a card in a specified color.
     * @param color
     * @return true if a cord in this color is in the hand, otherwise false 
     */
    public boolean hasColor(CardColor color) {
        for (Card card : this.cards) {
            if (card.getColor() == color) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param color
     * @return all cards of a specified color.
     */
    public List<Card> getCardsByColor(CardColor color) {

        List<Card> cardsByColor = new ArrayList<Card>();
        for (Card card : this.cards) {
            if (card.getColor() == color) {
                cardsByColor.add(card);
            }
        }
        return cardsByColor;
    }
}
