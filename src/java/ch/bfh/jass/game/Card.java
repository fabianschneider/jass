/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package ch.bfh.jass.game;

import ch.bfh.jass.game.JassCardSet.CardColor;
import ch.bfh.jass.game.JassCardSet.CardRank;
import ch.bfh.jass.interfaces.IPlayer;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
public class Card {

    private CardColor color;
    private CardRank rank;
    private IPlayer holder;
    private boolean inHand;

    /**
     * *
     * Class Constructor
     *
     * @param color
     * @param rank
     */
    public Card(CardColor color, CardRank rank) {
        this.color = color;
        this.rank = rank;
        this.inHand = false;
    }

    /**
     * Checks is dealed out.
     *
     * @return boolean
     */
    public boolean isInHand() {
        return this.inHand;
    }

    /**
     * Sets the card deale out.
     */
    public void dealedOut() {
        this.inHand = true;
    }

    /**
     * Resets a card.
     */
    public void resetCardFlags() {
        this.inHand = false;
        this.holder = null;
    }

    /**
     * @return the color of the card
     */
    public CardColor getColor() {
        return color;
    }

    /**
     * @return the rank of the card
     */
    public CardRank getRank() {
        return rank;
    }

    /**
     * @return the holder of the card
     */
    public IPlayer getHolder() {
        return holder;
    }

    /**
     * Set the holder of the card.
     * @param holder
     */
    public void setHolder(IPlayer holder) {
        this.holder = holder;
    }

    @Override
    public String toString() {
        return "Card{" + "color=" + color + ", value=" + rank + ", inHand=" + inHand + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if (this.color != other.color) {
            return false;
        }
        if (this.rank != other.rank) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.color.ordinal();
        hash = 17 * hash + this.rank.ordinal();
        return hash;
    }
}
