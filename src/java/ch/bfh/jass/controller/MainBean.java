package ch.bfh.jass.controller;

import ch.bfh.jass.exceptions.UserAlreadyExistsException;
import ch.bfh.jass.game.Player;
import ch.bfh.jass.interfaces.IPlayer;
import ch.bfh.jass.model.EntityManager;
import ch.bfh.jass.model.FinishedGame;
import ch.bfh.jass.model.MessageFactory;
import ch.bfh.jass.model.User;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * @author David Baumgartner <baumd9@bfh.ch>, Fabian Schneider <schnf6@bfh.ch>
 * @version 1.0
 */
@ManagedBean
@SessionScoped
public class MainBean implements Serializable {

    private EntityManager entityManager;
    private User user;
    private IPlayer player = new Player();
    private String name;
    private String username;
    private String password;
    private String passwordRepeat;
    private String locale;

    /**
     * Class Constructor
     * 
     * Gets the current instance of the entityManager
     */
    public MainBean() {
        this.entityManager = EntityManager.getCurrentInstance();
    }

    /**
     * @return the player
     */
    public IPlayer getPlayer() {
        return player;
    }

    /**
     * Set the player
     * @param player
     */
    public void setPlayer(IPlayer player) {
        this.player = player;
    }

    /**
     * @return the password of the user
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password of the user
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the username of the user
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the username of the user
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the full name of the user
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Set the full name of the user
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the password repetition
     */
    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    /**
     * Set the password repetition
     * @param passwordRepeat
     */
    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    /**
     * Get the user object
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the user object
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get the entityManager
     * @return
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Set the locale
     * @param locale
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Get the locale, if the locale is not set, return the locale of the FacesContext
     * @return
     */
    public String getLocale() {
        if (locale == null) {
            FacesContext context = FacesContext.getCurrentInstance();
            return context.getViewRoot().getLocale().toString();
        }
        return locale;
    }

    /**
     * Login to the system, error messages are generated if the login fails
     * @return
     */
    public String login() {

        try {
            user = entityManager.getUser(username);
            if (user == null || !user.getPassword().equals(entityManager.hashMD5(password))) {
                MessageFactory.error("loginFailed");
                return "login";
            }
        } catch (SQLException ex) {
            MessageFactory.error("loginFailed");
            return "login";
        }

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute("AUTH", true);
        this.player.setUsername(this.username);

        return "game";
    }

    /**
     * Register to the system, error messages for password check, user already exist and registration failed
     * @return
     */
    public String register() {
        if (this.username == null || this.password == null || this.name == null || this.passwordRepeat == null) {
            MessageFactory.error("registrationFailed");
            return "register";
        } else if (!this.password.equals(this.passwordRepeat)) {
            MessageFactory.error("passwordRepeat");
            return "register";
        }
        try {
            entityManager.createNewUser(this.username, this.password, this.name);
        } catch (SQLException ex) {
            MessageFactory.error("registrationFailed");
            return "register";
        } catch (UserAlreadyExistsException ex) {
            MessageFactory.error("userAlreadyExists");
            return "register";
        }
        return "login";
    }
    
    /**
     * Returns a list of all played games of the user.
     * @return
     * @throws SQLException 
     */
    public List<FinishedGame> getGameHistory() throws SQLException {
        if(this.user == null){
            return new ArrayList<FinishedGame>();
        }
        return this.entityManager.getGameHistoryByUser(this.user);
    }
    
    /**
     * Verfies if the mainbean contains a user.
     * @return 
     */
    public boolean isHasUser(){
        return this.user != null;
    }
}
